## Preparando servidor no Windows ##

*Postgre SQL –  Liberar conexões externas*

Por questões de segurança, o PostgreSQL é instalado com a configuração apenas para rodar em servidor local, fazendo-se necessária uma alteração em dois arquivos dentro da pasta de instalação do postgresql, normalmente está no caminho:
<br>
C:\Program Files\PostgreSQL\13\data 
- Note que **13** refere-se à versão instalada
Os arquivos a serem editados são “postgresql.conf” e “pg_hba.conf”.
Para alterar os arquivos, é necessário transformar sua extensão para TXT, mas antes é importante alterar a configuração de visibilidade do explorador de arquivos caso o sistema operacional seja Windows, da seguinte forma:
<br>

    1.	No explorador de arquivos, vá na aba “Arquivo” – “Alterar configurações de pasta e pesquisa”
    2.	Clique na aba “Modo de Exibição”
    3.	Procure “Ocultar as extensões dos tipos de arquivos conhecidos”
    4.	Caso a caixa desta opção esteja marcada, desmarque-a.
<br>
Feito isso, alterando os arquivos citados para a extensão txt, abra-as no bloco de notas e faça as seguintes alterações:
<br>

1.	No postgressql.conf, procure pela linha:
 #listen_addresses = "localhost" # what IP address(es) to listen on”
<br>
caso esteja assim,  altere-a  para:
listen_addresses = "*" # what IP address(es) to listen on
<br>
nota: o asterisco representa a possibilidade de leitura de qualquer endereço ip, entretanto ela também pode ler um endereço exclusivo.
<br>

2.	No pg_hba.conf, encontre as configurações de conexão e altere para:

 #### "local" is for Unix domain socket connections only
 <br>
local all all md5

 #### IPv4 local connections:
host all all 127.0.0.1/32 md5 
<br>
**host all all 0.0.0.0/0 md5**
<br>
#### IPv6 local connections:
host all all ::1/128 md5


Para liberar porta do firewall no windows, basta ir em: Painel de controle -> Sistrema e segurança -> Firewall do Windows. 
Feito isso, faça o seguinte procedimento:
<br>

    1.	Clique em Configurações avançadas
    2.	Regras de entrada
    3.	Nova regra
    4.	Regra por portas
    5.	Portas locais específicas (padrão: 5432)
    6.	Avançar até a hora de nomear o ponto de acesso específico.
